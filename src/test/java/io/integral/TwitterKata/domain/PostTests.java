package io.integral.TwitterKata.domain;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PostTests {

    @Test
    void getTimelineEntry_ReturnsTimelineEntryString() {
        String postText = "First first time posting!!!";
        LocalDateTime postTime = LocalDateTime.now();
        LocalDateTime currentTime = LocalDateTime.now().plusDays(1);

        Post post = new Post(postText, postTime);
        String timelineEntry = post.getTimelineEntry(currentTime);

        assertEquals(postText + " (1 day ago)", timelineEntry);
    }
}
