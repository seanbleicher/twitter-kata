package io.integral.TwitterKata.domain;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TimeDifferenceTests {

    @Test
    void TimeDifference_Seconds_Min(){
        LocalDateTime now = LocalDateTime.now();
        TimeDifference timeDifference = new TimeDifference(now, now.plusSeconds(1));

        assertEquals(timeDifference.getValue(), 1);
        assertEquals(timeDifference.getUnit(), "second");
    }

    @Test
    void TimeDifference_Seconds_Max(){
        LocalDateTime now = LocalDateTime.now();
        TimeDifference timeDifference = new TimeDifference(now, now.plusSeconds(59));

        assertEquals(timeDifference.getValue(), 59);
        assertEquals(timeDifference.getUnit(), "seconds");
    }

    @Test
    void TimeDifference_Minute_Min(){
        LocalDateTime now = LocalDateTime.now();
        TimeDifference timeDifference = new TimeDifference(now, now.plusSeconds(60));

        assertEquals(timeDifference.getValue(), 1);
        assertEquals(timeDifference.getUnit(), "minute");
    }

    @Test
    void TimeDifference_Minute_Max(){
        LocalDateTime now = LocalDateTime.now();
        TimeDifference timeDifference = new TimeDifference(now, now.plusMinutes(59));

        assertEquals(timeDifference.getValue(), 59);
        assertEquals(timeDifference.getUnit(), "minutes");
    }

    @Test
    void TimeDifference_Hours_Min(){
        LocalDateTime now = LocalDateTime.now();
        TimeDifference timeDifference = new TimeDifference(now, now.plusMinutes(60));

        assertEquals(timeDifference.getValue(), 1);
        assertEquals(timeDifference.getUnit(), "hour");
    }

    @Test
    void TimeDifference_Hours_Max(){
        LocalDateTime now = LocalDateTime.now();
        TimeDifference timeDifference = new TimeDifference(now, now.plusHours(23));

        assertEquals(timeDifference.getValue(), 23);
        assertEquals(timeDifference.getUnit(), "hours");
    }

    @Test
    void TimeDifference_Day_Min(){
        LocalDateTime now = LocalDateTime.now();
        TimeDifference timeDifference = new TimeDifference(now, now.plusHours(24));

        assertEquals(timeDifference.getValue(), 1);
        assertEquals(timeDifference.getUnit(), "day");
    }

    @Test
    void TimeDifference_Day_Max(){
        LocalDateTime now = LocalDateTime.now();
        TimeDifference timeDifference = new TimeDifference(now, now.plusDays(6));

        assertEquals(timeDifference.getValue(), 6);
        assertEquals(timeDifference.getUnit(), "days");
    }

    @Test
    void TimeDifference_Week_Min(){
        LocalDateTime now = LocalDateTime.now();
        TimeDifference timeDifference = new TimeDifference(now, now.plusDays(7));

        assertEquals(timeDifference.getValue(), 1);
        assertEquals(timeDifference.getUnit(), "week");
    }

    @Test
    void TimeDifference_Week_Max(){
        LocalDateTime now = LocalDateTime.now();
        TimeDifference timeDifference = new TimeDifference(now, now.plusWeeks(4));

        assertEquals(timeDifference.getValue(), 4);
        assertEquals(timeDifference.getUnit(), "weeks");
    }

    @Test
    void TimeDifference_Month_Min(){
        LocalDateTime now = LocalDateTime.now();
        TimeDifference timeDifference = new TimeDifference(now, now.plusMonths(1));

        assertEquals(timeDifference.getValue(), 1);
        assertEquals(timeDifference.getUnit(), "month");
    }

    @Test
    void TimeDifference_Month_Max(){
        LocalDateTime now = LocalDateTime.now();
        TimeDifference timeDifference = new TimeDifference(now, now.plusMonths(11));

        assertEquals(timeDifference.getValue(), 11);
        assertEquals(timeDifference.getUnit(), "months");
    }

    @Test
    void TimeDifference_Year_Min(){
        LocalDateTime now = LocalDateTime.now();
        TimeDifference timeDifference = new TimeDifference(now, now.plusMonths(12));

        assertEquals(timeDifference.getValue(), 1);
        assertEquals(timeDifference.getUnit(), "year");
    }

    @Test
    void TimeDifference_Years(){
        LocalDateTime now = LocalDateTime.now();
        TimeDifference timeDifference = new TimeDifference(now, now.plusYears(100));

        assertEquals(timeDifference.getValue(), 100);
        assertEquals(timeDifference.getUnit(), "years");
    }
}
