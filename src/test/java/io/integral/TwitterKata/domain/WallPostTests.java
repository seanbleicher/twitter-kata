package io.integral.TwitterKata.domain;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WallPostTests {

    @Test
    void getWallEntry_ReturnsWallEntryString() {
        String name = "Alice";
        String postText = "First first time posting!!!";
        LocalDateTime postTime = LocalDateTime.now();
        LocalDateTime currentTime = LocalDateTime.now().plusDays(1);

        Post post = new Post(postText, postTime);
        WallPost wallPost = new WallPost(name, post);
        String wallEntry = wallPost.getWallEntry(currentTime);

        assertEquals(name + " - " + postText + " (1 day ago)", wallEntry);
    }
}
