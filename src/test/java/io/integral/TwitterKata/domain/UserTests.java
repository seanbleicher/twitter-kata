package io.integral.TwitterKata.domain;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserTests {

    @Test
    void getTimeline_ReturnsPost(){
        User alice = new User("Alice");

        LocalDateTime now = LocalDateTime.now();
        alice.post("I love the weather today.", now);

        List<String> timeline = alice.getTimeline(now.plusSeconds(5));

        assertEquals(timeline.size(), 1);
        assertEquals(timeline.get(0), "I love the weather today. (5 seconds ago)");
    }

    @Test
    void getTimeline_ReturnsTwoPostWithDifferentTimes(){
        User alice = new User("Alice");

        LocalDateTime now = LocalDateTime.now();
        alice.post("I love the weather today.", now.plusMinutes(3));
        alice.post("First first time posting!!!", now);

        List<String> timeline = alice.getTimeline(now.plusHours(1));

        assertEquals(timeline.size(), 2);
        assertEquals(timeline.get(0), "I love the weather today. (57 minutes ago)");
        assertEquals(timeline.get(1), "First first time posting!!! (1 hour ago)");
    }

    @Test
    void getTimeline_SortsByTime(){
        User alice = new User("Alice");

        LocalDateTime now = LocalDateTime.now();
        alice.post("I love the weather today.", now);
        alice.post("First first time posting!!!", now.plusMinutes(3));

        List<String> timeline = alice.getTimeline(now.plusHours(1));

        assertEquals(timeline.size(), 2);
        assertEquals(timeline.get(0), "First first time posting!!! (57 minutes ago)");
        assertEquals(timeline.get(1), "I love the weather today. (1 hour ago)");

    }

    @Test
    void getUserTimeline_ReturnsUserPosts(){
        User alice = new User("Alice");
        User bob = new User("Bob");

        LocalDateTime now = LocalDateTime.now();
        bob.post("Good game though.", now.minusMinutes(1));
        bob.post("Darn! We lost!", now.minusMinutes(2));

        List<String> timeline = alice.getUserTimeline(bob, now);

        assertEquals(timeline.size(), 2);
        assertEquals(timeline.get(0), "Good game though. (1 minute ago)");
        assertEquals(timeline.get(1), "Darn! We lost! (2 minutes ago)");

    }

    @Test
    void getWall_ReturnsUserPlusFriendsPosts(){
        User alice = new User("Alice");
        User bob = new User("Bob");
        User charles = new User("Charles");

        LocalDateTime now = LocalDateTime.now();
        charles.post("I'm in New York today! Anyone wants to have a coffee?", now.minusSeconds(15));
        bob.post("Good game though.", now.minusMinutes(1));
        bob.post("Darn! We lost!", now.minusMinutes(2));
        alice.post("I love the weather today.", now.minusMinutes(5));

        charles.addFriend(alice);
        charles.addFriend(bob);

        List<String> timeline = charles.getWall(now);

        assertEquals(timeline.size(), 4);
        assertEquals(timeline.get(0), "Charles - I'm in New York today! Anyone wants to have a coffee? (15 seconds ago)");
        assertEquals(timeline.get(1), "Bob - Good game though. (1 minute ago)");
        assertEquals(timeline.get(2), "Bob - Darn! We lost! (2 minutes ago)");
        assertEquals(timeline.get(3), "Alice - I love the weather today. (5 minutes ago)");
    }
}
