package io.integral.TwitterKata.domain;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PostSorterTests {

    @Test
    void sortPosts_TimeUnitJumps(){
        LocalDateTime now = LocalDateTime.now();
        PostSorter postSorter = new PostSorter();

        Post post1 = new Post("Here is the test post.", now.minusHours(1));
        Post post2 = new Post("Here is the test post.", now.minusMinutes(1));
        Post post3 = new Post("Here is the test post.", now.minusSeconds(1));

        List<Post> posts = new ArrayList<>();
        posts.add(post1);
        posts.add(post2);
        posts.add(post3);
        postSorter.sortPosts(posts);

        assertEquals(posts.size(), 3);
        assertEquals(posts.get(0), post3);
        assertEquals(posts.get(1), post2);
        assertEquals(posts.get(2), post1);
    }

    @Test
    void sortPosts_TimeJumps(){
        LocalDateTime now = LocalDateTime.now();
        PostSorter postSorter = new PostSorter();

        Post post1 = new Post("Here is the test post.", now.minusSeconds(3));
        Post post2 = new Post("Here is the test post.", now.minusSeconds(2));
        Post post3 = new Post("Here is the test post.", now.minusSeconds(1));

        List<Post> posts = new ArrayList<>();
        posts.add(post1);
        posts.add(post2);
        posts.add(post3);
        postSorter.sortPosts(posts);

        assertEquals(posts.size(), 3);
        assertEquals(posts.get(0), post3);
        assertEquals(posts.get(1), post2);
        assertEquals(posts.get(2), post1);
    }

    @Test
    void sortWallPosts_TimeUnitJumps(){
        String name = "Alice";
        LocalDateTime now = LocalDateTime.now();
        PostSorter postSorter = new PostSorter();

        Post post1 = new Post("Here is the test post.", now.minusHours(1));
        Post post2 = new Post("Here is the test post.", now.minusMinutes(1));
        Post post3 = new Post("Here is the test post.", now.minusSeconds(1));

        WallPost wallPost1 = new WallPost(name, post1);
        WallPost wallPost2 = new WallPost(name, post2);
        WallPost wallPost3 = new WallPost(name, post3);

        List<WallPost> wallPosts = new ArrayList<>();
        wallPosts.add(wallPost1);
        wallPosts.add(wallPost2);
        wallPosts.add(wallPost3);
        postSorter.sortWallPosts(wallPosts);

        assertEquals(wallPosts.size(), 3);
        assertEquals(wallPosts.get(0), wallPost3);
        assertEquals(wallPosts.get(1), wallPost2);
        assertEquals(wallPosts.get(2), wallPost1);
    }

    @Test
    void sortWallPosts_TimeJumps(){
        String name = "Alice";
        LocalDateTime now = LocalDateTime.now();
        PostSorter postSorter = new PostSorter();

        Post post1 = new Post("Here is the test post.", now.minusSeconds(3));
        Post post2 = new Post("Here is the test post.", now.minusSeconds(2));
        Post post3 = new Post("Here is the test post.", now.minusSeconds(1));

        WallPost wallPost1 = new WallPost(name, post1);
        WallPost wallPost2 = new WallPost(name, post2);
        WallPost wallPost3 = new WallPost(name, post3);

        List<WallPost> wallPosts = new ArrayList<>();
        wallPosts.add(wallPost1);
        wallPosts.add(wallPost2);
        wallPosts.add(wallPost3);
        postSorter.sortWallPosts(wallPosts);

        assertEquals(wallPosts.size(), 3);
        assertEquals(wallPosts.get(0), wallPost3);
        assertEquals(wallPosts.get(1), wallPost2);
        assertEquals(wallPosts.get(2), wallPost1);
    }
}
