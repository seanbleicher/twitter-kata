package io.integral.TwitterKata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwitterKataApplication {

	public static void main(String[] args) {
		SpringApplication.run(TwitterKataApplication.class, args);
	}

}
