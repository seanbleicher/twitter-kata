package io.integral.TwitterKata.domain;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class TimeDifference {

    private String unit;
    private Long value;

    private final ChronoUnit[] timeUnits = new ChronoUnit[]{
            ChronoUnit.YEARS,
            ChronoUnit.MONTHS,
            ChronoUnit.WEEKS,
            ChronoUnit.DAYS,
            ChronoUnit.HOURS,
            ChronoUnit.MINUTES,
            ChronoUnit.SECONDS

    };

    public TimeDifference(LocalDateTime timestamp, LocalDateTime currentTime){

        for(ChronoUnit timeUnit : timeUnits){
            long timeDifference = timeUnit.between(timestamp, currentTime);
            if(timeDifference >= 1){
                value = timeDifference;
                unit = checkPlural(timeUnit.toString());
                break;
            }
        }
    }

    private String checkPlural(String baseUnit){
        if (value == 1){
            return baseUnit.substring(0, baseUnit.length() - 1).toLowerCase();
        }

        return baseUnit.toLowerCase();
    }

    public String getUnit(){
        return unit;
    }

    public Long getValue(){
        return value;
    }
}
