package io.integral.TwitterKata.domain;

import java.util.Comparator;

public class WallPostComparator implements Comparator {

    public int compare(Object o1, Object o2) {
        WallPost wallPost1 = (WallPost) o1;
        WallPost wallPost2 = (WallPost) o2;

        if (wallPost1.getPost().isBefore(wallPost2.getPost())) {
            return 1;
        }

        return -1;
    }
}
