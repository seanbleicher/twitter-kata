package io.integral.TwitterKata.domain;

import java.time.LocalDateTime;

public class Post {

    private String text;
    private LocalDateTime timestamp;

    public Post(String text, LocalDateTime time) {
        this.text = text;
        timestamp = time;
    }

    public String getTimelineEntry(LocalDateTime currentTime) {
        TimeDifference timeDifference = new TimeDifference(timestamp, currentTime);
        return text + " (" + timeDifference.getValue() + " " + timeDifference.getUnit() + " ago)";
    }

    public Boolean isBefore(Post post) {
        return timestamp.isBefore(post.timestamp);
    }
}
