package io.integral.TwitterKata.domain;

import java.util.Comparator;

public class PostComparator implements Comparator {

    public int compare(Object o1, Object o2) {
        Post post1 = (Post) o1;
        Post post2 = (Post) o2;

        if (post1.isBefore(post2)) {
            return 1;
        }

        return -1;
    }
}