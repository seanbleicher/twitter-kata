package io.integral.TwitterKata.domain;

import java.util.List;

public class PostSorter {

    PostComparator postComparator;
    WallPostComparator wallPostComparator;

    public PostSorter(){
        postComparator = new PostComparator();
        wallPostComparator = new WallPostComparator();
    }

    public void sortPosts(List<Post> posts){
        posts.sort(postComparator);
    }

    public void sortWallPosts(List<WallPost> posts){
        posts.sort(wallPostComparator);
    }
}
