package io.integral.TwitterKata.domain;

import java.time.LocalDateTime;

public class WallPost {
    String name;
    Post post;

    public WallPost(String name, Post post){
        this.name = name;
        this.post = post;
    }

    public String getWallEntry(LocalDateTime currentTime){
        return name + " - " + post.getTimelineEntry(currentTime);
    }

    public Post getPost(){
        return post;
    }
}
