package io.integral.TwitterKata.domain;

import java.time.LocalDateTime;
import java.util.*;

public class User {

    private String name;
    private List<Post> posts;
    private List<User> friends;
    private PostSorter postSorter;

    public User(String name) {
        this.name = name;
        posts = new ArrayList<>();
        friends = new ArrayList<>();
        postSorter = new PostSorter();
    }

    public void post(String text, LocalDateTime currentTime) {
        posts.add(new Post(text, currentTime));
        postSorter.sortPosts(posts);
    }

    public void addFriend(User friend){
        friends.add(friend);
    }

    public List<String> getTimeline(LocalDateTime currentTime) {
        List<String> timeline = new ArrayList<>();
        for (Post post : posts) {
            timeline.add(post.getTimelineEntry(currentTime));
        }

        return timeline;
    }

    public List<String> getUserTimeline(User user, LocalDateTime now) {
        return user.getTimeline(now);
    }

    public List<String> getWall(LocalDateTime currentTime){
        List<String> wall = new ArrayList<>();
        List<WallPost> wallPosts = new ArrayList<>();

        for (Post post : posts) {
            wallPosts.add(new WallPost(name, post));
        }

        for (User friend : friends) {
            String name = friend.getName();
            for (Post post : friend.getPosts()) {
                wallPosts.add(new WallPost(name, post));
            }
        }

        postSorter.sortWallPosts(wallPosts);

        for (WallPost wallPost : wallPosts) {
            wall.add(wallPost.getWallEntry(currentTime));
        }

        return wall;
    }

    public String getName(){
        return name;
    }

    public List<Post> getPosts(){
        return posts;
    }
}
